# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2019/08/19 20:00:00 by sabonifa          #+#    #+#              #
#    Updated: 2019/08/20 18:28:18 by sabonifa         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRC = draw/colors.c \
	  draw/draw_from_array.c \
	  draw/draw_in_octant.c \
	  draw/draw_line.c \
	  set_info/set_size.c \
	  set_info/window_init.c\
	  events/deal_key.c \
	  events/move.c \
	  map/check_file.c \
	  map/parse_file.c \
	  map/file_to_lst.c \
	  map/lst_to_map.c \
	  main.c

OBJ = $(SRC:.c=.o)

FLAGS = -Wall -Wextra -Werror

LIB = libft/libft.a
LFT_INC = libft/include
LFT = libft -lft
LX_INC = /usr/local/include
LX = /usr/local/lib -lmlx -framework OpenGL -framework AppKit

CC = gcc

all : $(NAME)

%.o : %.c $(SRC)
	@$(CC) $(FLAGS) -I . -I libft/includes -o $@ -c $<

force :
	@true

$(LIB) : force
	@echo "Make libft"
	@make -C libft/

$(NAME) : $(LIB) $(OBJ) fdf.h Makefile
	@echo "Making..."
	@$(CC) $(FLAGS) -I $(LFT_INC) -L $(LFT) -I $(LX_INC) -L $(LX) -I . -o $@ $(OBJ)
	@echo "Made !"

clean :
	@echo "Cleaning objects"
	@rm -rf $(OBJ)
	@make -C libft/ clean

fclean : clean
	@echo "Cleaning all"
	@rm -rf $(NAME)
	@make -C libft/ fclean

re : fclean all
.PHONY: all, clean, fclean, re, force
