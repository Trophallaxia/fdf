/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   move.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 14:15:55 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/19 15:00:53 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	change_proj(int key, t_info **p)
{
	if (key == 82)
	{
		(*p)->angle = 0;
		(*p)->alt = 0;
		(*p)->x0 = 50;
		(*p)->y0 = (*p)->z_max * (*p)->z_ratio * (*p)->range + 50;
	}
	if (key == 65)
	{
		(*p)->angle = M_PI_2;
		(*p)->alt = 0;
		(*p)->x0 = 50 + (*p)->y_size * (*p)->tile_size;
		(*p)->y0 = (*p)->z_max * (*p)->z_ratio * (*p)->range + 50;
	}
	if (key == 76)
	{
		(*p)->angle = M_PI_4 / 4;
		(*p)->alt = 0.5;
	}
	return (0);
}

int	change_origin(int key, t_info **info)
{
	if (key == 86)
		(*info)->x0 -= (*info)->x_ratio;
	if (key == 88)
		(*info)->x0 += (*info)->x_ratio;
	if (key == 84)
		(*info)->y0 += (*info)->y_ratio;
	if (key == 91)
		(*info)->y0 -= (*info)->y_ratio;
	return (0);
}

int	change_tile(int key, t_info **info)
{
	if (key == 69 && (*info)->tile_size < 1000)
		(*info)->tile_size += 5;
	else if (key == 78 && (*info)->tile_size > 5)
		(*info)->tile_size -= 5;
	return (0);
}

int	change_angle(int key, t_info **info)
{
	if (key == 83)
		(*info)->angle += M_PI_4 / 4;
	if (key == 85)
		(*info)->angle -= M_PI_4 / 4;
	return (0);
}

int	change_z(int key, t_info **info)
{
	if (key == 67)
		(*info)->z_ratio += 0.5;
	if (key == 75)
		(*info)->z_ratio -= 0.5;
	return (0);
}
