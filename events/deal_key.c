/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   deal_key.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/22 15:28:10 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 18:29:17 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		change_alt(int key, t_info **info)
{
	if (key == 89)
		(*info)->alt += 0.1;
	if (key == 92)
		(*info)->alt -= 0.1;
	return (0);
}

int		deal_key(int key, t_info *info)
{
	if (key == 53)
	{
		mlx_destroy_window(info->mlx, info->win);
		exit(0);
	}
	if (key == 91 || key == 84 || key == 88 || key == 86)
		change_origin(key, &info);
	if (key == 69 || key == 78)
		change_tile(key, &info);
	if (key == 85 || key == 83)
		change_angle(key, &info);
	if (key == 67 || key == 75)
		change_z(key, &info);
	if (key == 89 || key == 92)
		change_alt(key, &info);
	if (key == 82 || key == 65 || key == 76)
		change_proj(key, &info);
	redraw(info);
	return (0);
}
