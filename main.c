/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   test_main.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/06/30 11:47:43 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 19:51:46 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

/*
**Uncomment this part to test memory leaks
**__attribute ((destructor)) void end(void);
**void end(void){ while(1) ;}
*/

void	usage(void)
{
	ft_putstr("usage : ./fdf <file>\n");
	exit(0);
}

int		main(int ac, char **av)
{
	t_info	info;
	t_info	*info_p;
	int		**map;

	info_p = &info;
	if (ac != 2)
		usage();
	map = parse_file(av[1], &info_p);
	info_p->map = map;
	info_p = init(info_p);
	info_p->img.p = mlx_new_image(info_p->mlx, info_p->win_len,
		info_p->win_hei);
	info_p->img.str = mlx_get_data_addr(info_p->img.p, &(info_p->img.bpp),\
		&(info_p->img.s_l), &(info_p->img.end));
	draw_from_map(info_p->x_size, info_p->y_size, info_p);
	mlx_key_hook(info_p->win, deal_key, info_p);
	mlx_expose_hook(info_p->win, redraw, info_p);
	mlx_loop(info_p->mlx);
	return (0);
}
