/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   set_size.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/19 12:22:24 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 19:49:58 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#define BORDER 0

int		get_map_size(t_line_list *start, t_info **info)
{
	t_line_list	*current;
	int			i;

	current = start;
	i = 0;
	(*info)->x_size = ft_splitlen(current->split);
	while (current != NULL)
	{
		i++;
		current = current->next;
	}
	(*info)->y_size = i;
	if ((*info)->x_size == 1 && (*info)->y_size == 1)
	{
		ft_putstr("A point is not a map\n");
		exit(0);
	}
	return (0);
}

int		set_origin(int max_z, t_info **p)
{
	(*p)->x0 = BORDER + ((*p)->win_len - calc_point((*p)->x_size - 1, 0, *p).x\
			+ calc_point(0, (*p)->y_size - 1, *p).x) / 2\
				- calc_point(0, (*p)->y_size - 1, *p).x;
	(*p)->y0 = BORDER + max_z * (*p)->z_ratio * (*p)->range;
	return (0);
}

t_info	*set_window_size(t_info *p)
{
	double	t;

	if (p->x_size < 10 && p->y_size < 10)
		p->win_len = 640;
	else if (p->x_size < 20 && p->y_size < 20)
		p->win_len = 1280;
	else if (p->x_size < 60 && p->y_size < 60)
		p->win_len = 1920;
	else
		p->win_len = 2560;
	p->win_hei = (double)p->win_len * (double)9 / (double)16;
	t = (double)p->win_len / (double)(calc_point(p->x_size - 1, 0, p).x
			- calc_point(0, p->y_size - 1, p).x);
	p->tile_size *= t / (double)2;
	p->tile_size++;
	return (p);
}
