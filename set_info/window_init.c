/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   window_init.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 17:13:02 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 14:27:56 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_info	*norm_z(t_info *p)
{
	int range;

	range = p->z_max - p->z_min + calc_point(p->x_size - 1, p->y_size - 1, p).y;
	range = p->win_hei / range + 1;
	range = range < 1 ? 1 : range;
	p->range = range;
	return (p);
}

t_info	*window_init(t_info *p)
{
	p = set_window_size(p);
	p = norm_z(p);
	set_origin(p->z_max, &p);
	p->win = mlx_new_window(p->mlx, p->win_len, p->win_hei, "Awesome fdf");
	return (p);
}

void	write_commands(t_info *p)
{
	char	*s;
	int		i;

	i = 0;
	while (i < (90 * p->img.s_l))
	{
		if ((i % p->img.s_l) < 370 * 4)
		{
			p->img.str[i] = 0xB5;
			p->img.str[i + 1] = 0x31;
			p->img.str[i + 2] = 0x10;
		}
		i += 4;
	}
	s = "^: 8, v: 2, >: 6, <: 4";
	mlx_string_put(p->mlx, p->win, 10, 10, WHITE, s);
	s = "Zoom+: +, Zoom-: -, Z+: *, Z-: /";
	mlx_string_put(p->mlx, p->win, 10, 25, WHITE, s);
	s = "Alt+: 9 Alt-:7, Rot+: 3, Rot-: 1";
	mlx_string_put(p->mlx, p->win, 10, 40, WHITE, s);
	s = "Orthx: 0, Orthy: 2, Parallel: enter";
	mlx_string_put(p->mlx, p->win, 10, 55, WHITE, s);
}

t_info	*init(t_info *p)
{
	p->mlx = mlx_init();
	p->x_ratio = 43;
	p->y_ratio = 25;
	p->tile_size = 10;
	p->angle = M_PI / 8;
	p->z_ratio = 1;
	p->alt = 1;
	p->x0 = 0;
	p->y0 = 0;
	p = window_init(p);
	return (p);
}
