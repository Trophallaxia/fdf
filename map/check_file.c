/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 16:08:42 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 12:54:31 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	check_line_content(char **split)
{
	size_t	i;
	size_t	j;

	i = 0;
	while (split[i])
	{
		j = 0;
		if (split[i][j])
		{
			if (j == 0 && (split[i][j] == '+' || split[i][j] == '-'))
				j++;
			if (split[i][j] < '0' || split[i][j] > '9')
				return (ERR_CNT);
		}
		i++;
	}
	return (OK);
}

int	check_line(t_line_list *start)
{
	t_line_list *current;
	size_t		len;

	current = start;
	len = ft_splitlen(current->split);
	while (current != NULL)
	{
		if (ft_splitlen(current->split) != len)
			return (ERR_LEN);
		if (check_line_content(current->split) == ERR_CNT)
			return (ERR_CNT);
		current = current->next;
	}
	return (OK);
}
