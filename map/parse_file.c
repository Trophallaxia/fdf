/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_file.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/21 13:33:19 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 14:19:51 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	get_z(int **map, t_info **p)
{
	int	col;
	int	row;

	col = 0;
	row = 0;
	(*p)->z_max = 0;
	(*p)->z_min = 0;
	while (row < (*p)->y_size)
	{
		col = 0;
		while (col < (*p)->x_size)
		{
			if (map[row][col] > (*p)->z_max)
				(*p)->z_max = map[row][col];
			if (map[row][col] < (*p)->z_min)
				(*p)->z_min = map[row][col];
			col++;
		}
		row++;
	}
	return (0);
}

int	open_file(char *file)
{
	int	fd;

	fd = open(file, O_RDONLY);
	if (fd <= 0)
	{
		ft_putstr(strerror(errno));
		ft_putchar('\n');
		exit(0);
	}
	return (fd);
}

int	**parse_file(char *file, t_info **info)
{
	int			**map;
	int			check;
	int			fd;
	t_line_list	*start;

	fd = open_file(file);
	start = file_to_lst(fd);
	start = line_lst_rev(start);
	check = check_line(start);
	get_map_size(start, &(*info));
	map = lst_to_map((*info)->x_size, (*info)->y_size, start);
	get_z(map, info);
	close(fd);
	return (map);
}
