/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   file_to_lst.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 15:34:53 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 15:34:04 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void		write_error(int i)
{
	char	*tab[4];

	tab[0] = "Empty line\n";
	tab[1] = "Wrong character in line\n";
	tab[2] = "Error while reading the file\n";
	tab[3] = "Empty file\n";
	ft_putstr(tab[i]);
	exit(0);
}

void		check_line_gnl(char *line)
{
	int i;

	i = 0;
	if (*line == 0)
		write_error(0);
	while (line[i])
	{
		if (line[i] < 32 || line[i] > 127)
			write_error(1);
		i++;
	}
}

t_line_list	*line_lst_new(void)
{
	t_line_list	*new;

	new = (t_line_list*)malloc(sizeof(*new) * 1);
	return (new);
}

t_line_list	*line_lst_rev(t_line_list *current)
{
	t_line_list *buff;
	t_line_list	*next;

	next = NULL;
	buff = current->next;
	current->next = next;
	while (buff != NULL)
	{
		next = current;
		current = buff;
		buff = current->next;
		current->next = next;
	}
	return (current);
}

t_line_list	*file_to_lst(int fd)
{
	t_line_list	*current;
	int			gnl;
	char		*line;
	t_line_list *new;
	t_line_list *buff;

	current = NULL;
	gnl = 0;
	while ((gnl = get_next_line(fd, &line) > 0))
	{
		buff = current;
		new = line_lst_new();
		check_line_gnl(line);
		new->split = ft_strsplit(line, ' ');
		new->next = buff;
		current = new;
		free(line);
	}
	if (gnl == -1)
		write_error(2);
	if (gnl == 0 && current == NULL)
		write_error(3);
	return (current);
}
