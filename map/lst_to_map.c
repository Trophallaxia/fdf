/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lst_to_map.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/18 17:20:50 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 19:03:10 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	free_lst(t_line_list *s)
{
	int			i;
	t_line_list	*tp;

	while (s != NULL)
	{
		i = -1;
		while (s->split[++i])
			free(s->split[i]);
		free(s->split[i]);
		free(s->split);
		tp = s;
		s = s->next;
		free(tp);
	}
	free(s);
}

int		**lst_to_map(size_t col, size_t row, t_line_list *start)
{
	int			**map;
	t_line_list	*current;
	size_t		i;
	size_t		j;

	map = ft_2d_arinit(col, row);
	current = start;
	i = 0;
	j = 0;
	while (current != NULL)
	{
		j = 0;
		while (current->split[j])
		{
			map[i][j] = atoi(current->split[j]);
			j++;
		}
		current = current->next;
		i++;
	}
	free_lst(start);
	return (map);
}
