/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 16:38:08 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 19:04:28 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include <mlx.h>
# include <math.h>
# include <stdlib.h>
# include <fcntl.h>
# include <string.h>
# include <errno.h>
# include "libft.h"
# include "get_next_line.h"

# define OK 0
# define ERR_CNT -1
# define ERR_LEN -2
# define DARK_RED 0x8000FF
# define PURPLE 0x4400FF
# define DEEP_BLUE 0x0000FF
# define LIGHT_BLUE 0x00FFFF
# define GREEN 0x00FF00
# define LIGHT_GREEN 0xAAFF00
# define YELLOW 0xFFFF00
# define ORANGE 0xFFA200
# define BROWN 0xFF4D00
# define WHITE 0xFFFFFF

typedef struct			s_img
{
	void				*p;
	char				*str;
	int					bpp;
	int					s_l;
	int					end;
}						t_img;

typedef struct			s_info
{
	void				*mlx;
	void				*win;
	int					tile_size;
	double				angle;
	double				alt;
	double				z_ratio;
	int					x0;
	int					y0;
	int					x_size;
	int					y_size;
	int					z_max;
	int					z_min;
	int					range;
	int					win_len;
	int					win_hei;
	int					x_ratio;
	int					y_ratio;
	int					**map;
	t_img				img;
}						t_info;

typedef struct			s_point
{
	int					x;
	int					y;
	int					z;
	int					yz;
}						t_point;

typedef struct			s_line_list
{
	char				**split;
	struct s_line_list	*next;
}						t_line_list;

/*
**Parsing functions
*/

int						check_line_content(char **split);
int						check_line(t_line_list *start);
int						open_file(char *file);
int						**parse_file(char *file, t_info **info);
char					**ft_split_whitespaces(char *str);

/*
**Settings functions
*/

int						get_map_size(t_line_list *start, t_info **info);
int						set_origin(int max_z, t_info **info);
t_info					*set_window_size(t_info *info);
t_info					*window_init(t_info *info);
t_info					*init(t_info *p);

/*
**List manpulation functions
*/

t_line_list				*line_lst_new(void);
t_line_list				*line_lst_rev(t_line_list *current);
t_line_list				*file_to_lst(int fd);
int						**lst_to_map(size_t col,
							size_t row, t_line_list *start);

/*
**Draw functions
*/

void					ft_swap_point(t_point *p1, t_point *p2);
int						draw_1st_oct(t_info *info_p, t_point *p1, t_point *p2);
int						draw_2nd_oct(t_info *info_p, t_point *p1, t_point *p2);
int						draw_8th_oct(t_info *info_p, t_point *p1, t_point *p2);
int						draw_7th_oct(t_info *info_p, t_point *p1, t_point *p2);
int						draw_right(t_info *info_p, t_point *p1, t_point *p2);
int						draw_left(t_info *info_p, t_point *p1, t_point *p2);
int						draw_line(t_info *info_p, t_point *p1, t_point *p2);
int						draw_from_array(int row, int size, t_info *info);
int						draw_from_map(int col, int row, t_info *info);
int						draw_last_line(int row, int size, t_info *info);
int						redraw(t_info *info);
t_point					calc_point(int col, int row, t_info *ip);
void					write_commands(t_info *info);
int						color(int z);
int						calc(double k, int c1, int c2);
int						gradient(int z, t_point *p1,
							t_point *p2, t_info *info_p);
void					fill_img(t_info *p, int x, int y, int color);
int						alt(t_point *p1, t_point *p2, t_point *p, t_info *ptr);

/*
**Events functions
*/

int						deal_key(int key, t_info *info);
int						change_origin(int key, t_info **info);
int						change_proj(int key, t_info **p);
int						change_origin(int key, t_info **info);
int						change_tile(int key, t_info **info);
int						change_angle(int key, t_info **info);
int						change_z(int key, t_info **info);
#endif
