/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 18:36:48 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/12 16:58:21 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memmove(void *dst, const void *src, size_t len)
{
	size_t	i;
	char	*tp_dst;
	char	*tp_src;

	tp_dst = (char*)dst;
	tp_src = (char*)src;
	i = 0;
	if (dst <= src)
	{
		while (i < len)
		{
			tp_dst[i] = tp_src[i];
			i++;
		}
	}
	else
	{
		while (len--)
			tp_dst[len] = tp_src[len];
	}
	return ((void*)dst);
}
