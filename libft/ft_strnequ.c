/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnequ.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 14:34:42 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 14:52:57 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

int	ft_strnequ(char const *s1, char const *s2, size_t n)
{
	size_t	i;
	char	*tp1;
	char	*tp2;

	i = 0;
	if (!s1 || !s2)
		return (0);
	tp1 = (char*)s1;
	tp2 = (char*)s2;
	if (!s1 && !s2)
		return (0);
	while (tp1[i] && tp2[i] && i < n && tp1[i] == tp2[i])
		i++;
	if (i == n)
		return (1);
	return ((tp1[i] - tp2[i]) == 0 ? 1 : 0);
}
