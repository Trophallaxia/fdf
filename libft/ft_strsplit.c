/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 17:43:57 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 15:04:59 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

static size_t	ft_count_words(char *str, char c)
{
	size_t	nb_wrd;
	char	*tp;

	nb_wrd = 0;
	tp = str;
	while (*tp == c && *tp)
		tp++;
	while (*tp)
	{
		nb_wrd++;
		tp++;
		while (*tp && *tp != c)
			tp++;
		while (*tp && *tp == c)
			tp++;
	}
	return (nb_wrd);
}

static size_t	ft_count_letters(char *str, char c)
{
	size_t	nb_let;
	char	*tp;

	nb_let = 0;
	tp = str;
	while (*tp && *tp != c)
	{
		nb_let++;
		tp++;
	}
	return (nb_let);
}

static char		*ft_sub_split(char const *s, unsigned int start, size_t len)
{
	char	*sub;
	size_t	i;

	i = 0;
	if (!(sub = (char*)malloc(sizeof(*sub) * (len + 1))))
		return (NULL);
	while (i < len)
	{
		sub[i] = s[start + i];
		i++;
	}
	sub[i] = 0;
	return (sub);
}

char			**ft_strsplit(char const *s, char c)
{
	char	**tab;
	size_t	i;
	size_t	j;

	i = 0;
	j = 0;
	if (!s)
		return (NULL);
	if (!(tab = (char**)malloc(sizeof(*tab)
					* (ft_count_words((char*)s, c) + 1))))
		return (NULL);
	while (i < ft_count_words((char*)s, c))
	{
		while (s[j] && s[j] == c)
			j++;
		if (!(tab[i] = ft_sub_split(s, j, ft_count_letters((char*)&s[j], c))))
			return (NULL);
		j += ft_count_letters((char*)&s[j], c);
		while (s[j] && s[j] == c)
			j++;
		i++;
	}
	tab[i] = 0;
	return (tab);
}
