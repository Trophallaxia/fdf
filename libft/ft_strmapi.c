/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 14:18:26 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/14 14:51:03 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

char			*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	unsigned int	s_len;
	unsigned int	i;
	char			*dup;

	if (!s || !f)
		return (NULL);
	i = 0;
	s_len = ft_strlen((char*)s);
	if (!(dup = (char*)malloc(sizeof(*dup) * (s_len + 1))))
		return (NULL);
	while (i < s_len)
	{
		dup[i] = f(i, s[i]);
		i++;
	}
	dup[i] = 0;
	return (dup);
}
