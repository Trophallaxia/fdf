/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/07 18:44:55 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 13:01:39 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

void	*ft_memchr(const void *s, int c, size_t n)
{
	unsigned char	*tp;

	tp = (unsigned char*)s;
	while (n--)
	{
		if (*tp == (unsigned char)c)
			return ((void*)tp);
		tp++;
	}
	return (NULL);
}
