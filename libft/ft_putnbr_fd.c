/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_fd.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 19:30:30 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/10 14:11:36 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>

void	ft_putnbr_fd(int n, int fd)
{
	long	nb;
	long	i;
	long	pwr;
	char	str[12];

	nb = (long)n;
	i = 0;
	pwr = 1;
	if (nb < 0)
	{
		nb = -nb;
		str[i] = '-';
		i++;
	}
	if (nb == 0)
		pwr *= 10;
	while (nb / pwr > 0)
		pwr *= 10;
	while (pwr /= 10)
		str[i++] = (nb / pwr) % 10 + '0';
	str[i] = 0;
	write(fd, str, i);
}
