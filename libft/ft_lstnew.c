/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstnew.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/10 14:58:59 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/13 16:35:09 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libft.h"

t_list	*ft_lstnew(void const *content, size_t content_size)
{
	t_list	*ls_new;

	if (!(ls_new = (t_list*)malloc(sizeof(*ls_new) * 1)))
		return (NULL);
	ls_new->content = NULL;
	ls_new->content_size = 0;
	ls_new->next = NULL;
	if (!content || content_size == 0)
		return (ls_new);
	if (!(ls_new->content = malloc(content_size)))
		return (NULL);
	ft_memcpy(ls_new->content, content, content_size);
	ls_new->content_size = content_size;
	return (ls_new);
}
