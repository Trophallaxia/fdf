/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/29 14:10:55 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/30 10:29:40 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "get_next_line.h"

static int	read_fd(int fd, char **str)
{
	char	buff[BUFF_SIZE + 1];
	int		ret;
	char	*tmp;

	ret = read(fd, buff, BUFF_SIZE);
	buff[ret] = 0;
	tmp = *str;
	*str = ft_strjoin(*str, buff);
	free(tmp);
	return (ret);
}

static char	*get_line(char **str)
{
	char	*tmp;
	char	*c;
	char	*line;

	if ((c = ft_strchr(*str, '\n')))
	{
		line = ft_strsub(*str, 0, c - *str);
		tmp = *str;
		*str = ft_strdup(c + 1);
		free(tmp);
	}
	else
	{
		line = ft_strdup(*str);
		ft_strclr(*str);
	}
	return (line);
}

int			get_next_line(const int fd, char **line)
{
	static char	*str[10240];
	int			ret;
	int			i;

	i = 0;
	if (fd < 0 || !line || BUFF_SIZE == 0 || read(fd, NULL, 0) == -1)
		return (-1);
	if (!str[fd])
		str[fd] = ft_strnew(0);
	while (!ft_strchr(str[fd], '\n') && (ret = read_fd(fd, &str[fd])) > 0)
	{
		i++;
	}
	if (*str[fd])
	{
		*line = get_line(&str[fd]);
		return (1);
	}
	return (0);
}
