/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_arinit.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/16 12:00:09 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 18:07:49 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int	*ft_arinit(size_t size)
{
	int		*array;
	size_t	i;

	i = 0;
	array = (int*)malloc(sizeof(*array) * size);
	while (i < size)
	{
		array[i] = 0;
		i++;
	}
	return (array);
}
