/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncat.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/08 11:43:15 by sabonifa          #+#    #+#             */
/*   Updated: 2018/11/08 11:50:08 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <string.h>

char	*ft_strncat(char *s1, const char *s2, size_t n)
{
	char	*tp1;
	char	*tp2;

	tp1 = s1;
	tp2 = (char*)s2;
	while (*tp1)
		tp1++;
	while (n-- > 0 && *tp2)
	{
		*tp1 = *tp2;
		tp1++;
		tp2++;
	}
	*tp1 = 0;
	return (s1);
}
