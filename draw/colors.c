/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   colors.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/08/19 13:07:59 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 14:26:46 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		color(int z)
{
	if (z > 100)
		return (WHITE);
	if (z > 80)
		return (BROWN);
	if (z > 60)
		return (ORANGE);
	if (z > 40)
		return (YELLOW);
	if (z > 20)
		return (LIGHT_GREEN);
	if (z >= 0)
		return (GREEN);
	if (z > -30)
		return (LIGHT_BLUE);
	if (z > -60)
		return (DEEP_BLUE);
	if (z > -90)
		return (PURPLE);
	return (DARK_RED);
}

int		alt(t_point *p1, t_point *p2, t_point *p, t_info *ptr)
{
	t_point min;
	t_point max;
	int		res;

	min = p1->z < p2->z ? *p1 : *p2;
	max = p1->z < p2->z ? *p2 : *p1;
	if (p1->z == p2->z)
		return (p1->z);
	res = min.z + (min.yz - p->y) / (ptr->range * ptr->z_ratio);
	return (res < max.z ? res : max.z);
}

void	fill_img(t_info *p, int x, int y, int color)
{
	int i;

	i = x * 4 + y * (p->img.s_l);
	p->img.str[i] = color & 0xFF;
	p->img.str[i + 1] = (color & 0xFF00) >> 8;
	p->img.str[i + 2] = (color & 0xFF0000) >> 16;
	p->img.str[i + 3] = 0;
}
