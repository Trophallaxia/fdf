/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 10:21:15 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/19 13:07:31 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	ft_swap_point(t_point *p1, t_point *p2)
{
	t_point p3;

	p3.x = p1->x;
	p3.y = p1->y;
	p1->x = p2->x;
	p1->y = p2->y;
	p2->x = p3.x;
	p2->y = p3.y;
}

int		draw_right(t_info *info_p, t_point *p1, t_point *p2)
{
	int dx;
	int dy;

	dx = p2->x - p1->x;
	dy = p2->yz - p1->yz;
	if (dy >= 0)
	{
		if (dy <= dx)
			draw_1st_oct(info_p, p1, p2);
		else
			draw_2nd_oct(info_p, p1, p2);
	}
	else
	{
		if (-dy <= dx)
			draw_8th_oct(info_p, p1, p2);
		else
			draw_7th_oct(info_p, p1, p2);
	}
	return (0);
}

int		draw_left(t_info *info_p, t_point *p1, t_point *p2)
{
	draw_right(info_p, p2, p1);
	return (0);
}

int		draw_line(t_info *info_p, t_point *p1, t_point *p2)
{
	int dx;

	dx = p2->x - p1->x;
	if (dx >= 0)
		draw_right(info_p, p1, p2);
	else
		draw_left(info_p, p1, p2);
	return (0);
}
