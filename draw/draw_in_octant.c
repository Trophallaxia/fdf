/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_in_octant.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/02 15:28:11 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/20 14:03:38 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int	in_windows(int x, int y, t_info *p)
{
	if (x < 0 || x > p->win_len - 1)
		return (0);
	if (y < 0 || y > p->win_hei - 1)
		return (0);
	return (1);
}

int	draw_1st_oct(t_info *info_p, t_point *p1, t_point *p2)
{
	int		dy;
	int		e;
	int		dx;
	t_point p;

	dy = 2 * (p2->yz - p1->yz);
	e = p2->x - p1->x;
	dx = e * 2;
	p.x = p1->x;
	p.y = p1->yz;
	p.z = p1->z;
	while (p.x <= p2->x)
	{
		if (in_windows(p.x, p.y, info_p))
			fill_img(info_p, p.x, p.y, color(alt(p1, p2, &p, info_p)));
		if ((e = e - dy) <= 0)
		{
			p.y++;
			e = e + dx;
		}
		p.x++;
	}
	return (0);
}

int	draw_2nd_oct(t_info *info_p, t_point *p1, t_point *p2)
{
	int		dx;
	int		e;
	int		dy;
	t_point	p;

	dx = 2 * (p2->x - p1->x);
	e = p2->yz - p1->yz;
	dy = e * 2;
	p.x = p1->x;
	p.y = p1->yz;
	p.z = p1->z;
	while (p.y <= p2->yz)
	{
		if (in_windows(p.x, p.y, info_p))
			fill_img(info_p, p.x, p.y, color(alt(p1, p2, &p, info_p)));
		if ((e = e - dx) <= 0)
		{
			p.x++;
			e = e + dy;
		}
		p.y++;
	}
	return (0);
}

int	draw_8th_oct(t_info *info_p, t_point *p1, t_point *p2)
{
	int		dy;
	int		e;
	int		dx;
	t_point	p;

	dy = -2 * (p2->yz - p1->yz);
	e = p2->x - p1->x;
	dx = e * 2;
	p.x = p1->x;
	p.y = p1->yz;
	p.z = p1->z;
	while (p.x <= p2->x)
	{
		if (in_windows(p.x, p.y, info_p))
			fill_img(info_p, p.x, p.y, color(alt(p1, p2, &p, info_p)));
		if ((e = e - dy) <= 0)
		{
			p.y--;
			e = e + dx;
		}
		p.x++;
	}
	return (0);
}

int	draw_7th_oct(t_info *info_p, t_point *p1, t_point *p2)
{
	int		dx;
	int		e;
	int		dy;
	t_point p;

	dx = 2 * (p2->x - p1->x);
	e = -p2->yz + p1->yz;
	dy = e * 2;
	p.x = p1->x;
	p.y = p1->yz;
	p.z = p1->z;
	while (-p.y <= -p2->yz)
	{
		if (in_windows(p.x, p.y, info_p))
			fill_img(info_p, p.x, p.y, color(alt(p1, p2, &p, info_p)));
		if ((e = e - dx) <= 0)
		{
			p.x++;
			e = e + dy;
		}
		p.y--;
	}
	return (0);
}
