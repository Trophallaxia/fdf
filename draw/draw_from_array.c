/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_from_array.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sabonifa <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/07/09 14:22:53 by sabonifa          #+#    #+#             */
/*   Updated: 2019/08/19 21:28:47 by sabonifa         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	calc_point(int col, int row, t_info *p)
{
	t_point	pt;

	pt.x = p->x0;
	pt.x += p->tile_size * (col * cos(p->angle) - row * sin(p->angle));
	pt.y = p->tile_size * (col * sin(p->angle) + row * cos(p->angle));
	pt.y *= p->alt;
	pt.y += p->y0;
	pt.z = p->map[row][col];
	pt.yz = pt.y - pt.z * p->z_ratio * p->range;
	return (pt);
}

int		draw_from_array(int row, int size, t_info *info)
{
	int		i;
	t_point pt1;
	t_point pt2;

	i = 0;
	while (i + 1 < size)
	{
		pt1 = calc_point(i, row, info);
		pt2 = calc_point(i + 1, row, info);
		draw_line(info, &pt1, &pt2);
		pt2 = calc_point(i, row + 1, info);
		draw_line(info, &pt1, &pt2);
		i++;
	}
	pt1 = calc_point(i, row, info);
	pt2 = calc_point(i, row + 1, info);
	draw_line(info, &pt1, &pt2);
	return (0);
}

int		draw_last_line(int row, int size, t_info *info)
{
	int		i;
	t_point pt1;
	t_point pt2;

	i = 0;
	while ((i + 1) < size)
	{
		pt1 = calc_point(i, row, info);
		pt2 = calc_point(i + 1, row, info);
		draw_line(info, &pt1, &pt2);
		i++;
	}
	return (0);
}

int		redraw(t_info *p)
{
	mlx_destroy_image(p->mlx, p->img.p);
	p->img.p = mlx_new_image(p->mlx, p->win_len, p->win_hei);
	p->img.str = mlx_get_data_addr(p->img.p, &(p->img.bpp), \
			&(p->img.s_l), &(p->img.end));
	mlx_clear_window(p->mlx, p->win);
	draw_from_map(p->x_size, p->y_size, p);
	write_commands(p);
	return (0);
}

int		draw_from_map(int col, int row, t_info *p)
{
	size_t	i;

	i = 0;
	while (i + 1 < (size_t)row)
	{
		draw_from_array(i, col, p);
		i++;
	}
	draw_last_line(i, col, p);
	write_commands(p);
	mlx_put_image_to_window(p->mlx, p->win, p->img.p, 0, 0);
	return (0);
}
